const tabs = document.querySelectorAll('.tabs-title');
const contentItems = document.querySelectorAll('.tabs-content > li');
let activeTab = null;

const hideContent = () => {
  contentItems.forEach(contentItem => {
    contentItem.style.display = 'none';
  });
};

const showDefaultTab = () => {
  const defaultTab = tabs[0]; 
  defaultTab.classList.add('active');
  const defaultTabName = defaultTab.getAttribute('data-tab');
  const defaultContentItem = document.querySelector(`.tabs-content > li[data-tab="${defaultTabName}"]`);
  defaultContentItem.style.display = 'block';
  activeTab = defaultTab;
};

hideContent();
showDefaultTab();

tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    if (activeTab) {
      activeTab.classList.remove('active');
    }
    tab.classList.add('active');
    hideContent();

    const tbName = tab.getAttribute('data-tab');
    const activeContentItem = document.querySelector(`.tabs-content > li[data-tab="${tbName}"]`);
    activeContentItem.style.display = 'block';

    activeTab = tab;
  });
});